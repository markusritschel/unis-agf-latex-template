\documentclass[12pt,a4paper,notitlepage]{scrartcl}
\usepackage{amsmath}
\usepackage{multicol}

%BEGIN_FOLD Preamble
\usepackage[]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}
\AfterEndEnvironment{minted}{\bigskip}
\setminted[tex]{
	frame=lines,
	framesep=3mm,
	baselinestretch=1.2,
	bgcolor=code_bg,
	tabsize=4,
	breaklines,
	}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{csquotes}
%\usepackage{amsmath}
\usepackage{booktabs} 				% Is needed for \toprule f.e. in tables
%\usepackage{array}					% Extending the array and tabular environments
%\usepackage{setspace}				% Set space between lines (\onehalfspacing, \doublespacing)
%\usepackage{cancel}				% Strike out things with \cancel{tostrikeout}
\usepackage[usenames,dvipsnames]{xcolor}
%\usepackage{geometry}				% Set the page differently to the default settings
%\usepackage{amsfonts}
%\usepackage[cc]{titlepic}			% Enables ONE picture on titlepage
%\usepackage{amssymb}
\usepackage{graphicx}
%\usepackage{tabularx}
%\usepackage{pifont}
%\usepackage{marvosym}
%\usepackage{fontawesome}
%\usepackage{cite}
%\usepackage[automark]{scrpage2}
%\usepackage{scrlayer-scrpage}
%\usepackage{float}
\usepackage{caption}
%\usepackage{subcaption}
\captionsetup{justification=centering}
%\usepackage{makeplot}
%\usepackage[toc,page]{appendix} 	% for making an appendix
\usepackage[hyphens]{url} 	 		% prints URLs like they should be in bibtex
%\usepackage{wrapfig}				% Floating figures
% \usepackage{abstract} 				% adds the word "Abstract" and modifications can be made
\usepackage{hyperref}				% Makes active (on click) references in the final pdf
\hypersetup{
	colorlinks=true,
}
\usepackage{cleveref}
\usepackage{nameref}
%\usepackage{afterpage}				% Execute command after the next page break
%\usepackage{placeins}				% Control float placement with /FloatBarrier
%\pagestyle{scrheadings}
%\setheadsepline{1pt}
\clubpenalties 3 1000 150 0
\widowpenalties 3 1000 150 0
\usepackage{verbatim}
\setlength{\parindent}{0pt}


\usepackage{siunitx}				% Package to typeset SI units
% \usepackage{subfigure}
\definecolor{code_bg}{rgb}{0.95, 0.95, 0.95}
%END_FOLD Preamble


\title{Readme Students}
%\author{Markus Ritschel}
\date{\today}

%===========================================================================================

\begin{document}


\maketitle
\tableofcontents
\vspace{1cm}

This README provides you with the necessary information about the \LaTeX\ template and how to use it to write your report. Every group of students who shared one project during the fieldwork are responsible for one chapter of the report and will work in one of the \textbf{studentxx} directories located in the main directory.

\section{Structure}
The \textbf{studentxx} directory contains the following elements:

\begin{itemize}
	\item \textbf{main.tex}: the \LaTeX\ file to compile, while working on your chapter. 
	This can contain all the \LaTeX\ code of your report part or you can include subfiles which are located in your studentxx directory or even in subdirectories of it. \\
	Use \verb|\subinput{filename}| for including these files.
	\item \textbf{images} contains all the figures you want to include in your report part.\\
	More information on page~\pageref{sec:figures}.
	\item \textbf{bibliography.bib} contains all the literature references you want to cite in your chapter.\\
	More information on page~\pageref{sec:citations}.
	\item \textbf{appendix.tex}: anything you want to put in the appendix goes in here.\\
	You can include this file in your \textbf{main.tex} via \verb|\subappendix{appendix}|. 
	This will add the appendix at the end of your individual pdf.
	In the final report it will show up at the very end.
\end{itemize}

For compiling the \LaTeX\ code, it is important that you keep the structure you find in the \href{https://bitbucket.org/markusritschel/unis-agf-latex-template/}{Git repository} or on the UNIS server, i.e. that you have at least the \textbf{master} and your \textbf{studentxx} directory, since the latter refers to the \textit{master}.\\
In the main directory you also find a \textbf{Makefile} which has several routines helping you building the final pdf:
Just execute \verb|make latex d=studentxx| (with \verb|studentxx| being the name of your working directory) in your terminal to compile your report part.
The same can be done with \verb|d=master| for the complete report.

Of course you can also compile your \textbf{main.tex} with the compiler of your choice:
For Windows and Mac there is a large variety of (mostly free) software tools to do so, e.g. TeXworks, TeXstudio, TeXmaker etc. On Unix systems (Linux, Mac) you can also simply compile via command line (\verb|pdflatex main.tex|). 
Always compile your file at least two times so that \LaTeX\ can actually find all your references!


\section{Elements}
\subsection{Figures}
\label{sec:figures}

Every figure you want to implement goes into the \textbf{images} folder within your directory.
Use the following code snippet, to include a picture. 
You do \textbf{not} need to give any file paths, just the file name will be enough. 
\LaTeX\ will search for it in your \textbf{images} directory.

\begin{minted}{tex}
\begin{figure}[ht]
\centering
\includegraphics[trim=0mm 0mm 0mm 0mm, clip, width=1\textwidth]{filename}
\caption{Here is the caption for the figure.}
\label{fig:labelname}
\end{figure}
\end{minted}

You only need to replace \verb#filename# by your picture's name and drop your picture in the \textbf{images} subdirectory of your \textbf{studentxx} directory. 
If you want to change your picture you can trim/crop it with the first given argument or change the size by scaling it relatively to the textwidth (where \verb|1\textwidth| should be your maximum, that means just change 1 into 0.8 for example to get your picture smaller). 
You can also add \verb|rotate=90| to rotate a picture (in this case by 90 degrees counterclockwise).
Then you only need to add a caption and a label (see further explanation to labels below) and that's it.

\textbf{Hint:} If you want your floating environments (figures, tables, etc.) to be shown up to a certain point in the document, just add a \verb|\FloatBarrier|.
This will ensure that all floats that were given in the code up to that point will actually placed up to there and not moved any further.

\subsection{References and Labels}
\label{sec:ref_labels}

One of \LaTeX's strengths lies in the handling of references. 
Every object you might want to refer to in your report (e.g.\ a section, subsection, figure, table, chapter, etc.), you can label. 
Whenever you want to refer to this object you just call its labelname at the desired spot. 
Let's look at an example. 

\begin{table}[htbp]
\centering
	\begin{tabular}{lll}
		\toprule
		A & B & C \\
		\midrule
		d & e & f \\
		g & h & i \\
		\bottomrule
	\end{tabular}
\caption[Short caption]{This is the caption of the table.}
\label{tab:testtable}
\end{table}

\begin{minted}{tex}
\begin{table}[htbp]
\centering
	\begin{tabular}{lll}
	\toprule
	A & B & C \\
	\midrule
	d & e & f \\
	g & h & i \\
	\bottomrule
	\end{tabular}
\caption[Short caption]{This is the caption of the table.}
\label{tab:testtable}
\end{table}
\end{minted}

As you can see, the label is \verb|tab:testtable| so if we now want to refer to it, we just write \verb|\autoref{tab:testtable}| and the result looks like that: \autoref{tab:testtable}.

References can be made with several different commands.
Herefore the commands of the packages \texttt{hyperref}, \texttt{cleveref}, and \texttt{nameref} are available.
For example, one could refer to this section (\verb|\label{sec:ref_labels}|) as 
\begin{itemize}
	\item \verb|\autoref{sec:ref_labels}| which gives: \autoref{sec:ref_labels}
	\item \verb|\cref{sec:ref_labels}| which gives: \cref{sec:ref_labels}
	\item \verb|\Cref{sec:ref_labels}| which gives: \Cref{sec:ref_labels}
	\item \verb|\pageref{sec:ref_labels}| which gives: \pageref{sec:ref_labels}
	\item \verb|\nameref{sec:ref_labels}| which gives: \nameref{sec:ref_labels}.
\end{itemize}
Equations are best referred to with \verb|\eqref{eq:label}| which yields, for example, (\textcolor{red}{2.4}).\\

\textbf{Important information:}\\
Please do not confuse a \textbf{reference} with a \textbf{citation} of a report, article, etc.\ that has already been published! 
You will get information about the latter very soon.\\

Just another word on the labels: use an easy and consistent way of labeling!
Otherwise it will be hard for you or others to read your code later again.
The label should contain the type of object (table, figure, section, etc.), your token (because two people might call a figure \enquote{temperature03pm}) and the actual name of the object. 
An example: \textit{Student03} might call a figure about a timeseries of temperature \texttt{fig:03/temp\_timeseries}. 
Please get in touch with the editor about the labeling system beforehand!\\

You might also want to refer to another chapter. 
Let's say you want to student04. 
Then you would need to refer to that report chapter which in best case is labeled like \verb|\label{student04:report}|. Refer to it by using \verb|\autoref{student04:report}|. 
If you compile your report and this reference prints out as \textbf{??}, don't worry. 
This is due to the fact that \LaTeX\ doesn't know student04 yet because you are only compiling your report part. 
The editors will take care of this when compiling all the reports together in the end.

\subsection{Citations}
\label{sec:citations}

You will have to cite previous studies or articles that are related to your topic in your report. For this purpose, \LaTeX\ comes with some helpful commands: \verb#\textcite{}# for in-text citations, \verb#\parencite{}# for citations inside parantheses and \verb|\footcite{}| for footnote citations. 
Let's look at an example again. Lilalu and collegues wrote, that green is a color and you want to state that, too. So you can choose one of the following options:\\

\begin{minted}{tex}
\textcite{lilalu} stated, that green is a color.
\end{minted}

would result in: Lilalu et al.\ (2016) stated, that green is a color. \\

\begin{minted}{tex}
One has to consider, that green is a color \parencite{lilalu}.
\end{minted}

would give: One has to consider, that green is a color (Lilalu et al., 2016). The citation comes in brackets after the statement.\\

But of course, \LaTeX\ needs to know, what kind of literature Lilalu produced and where and when it was published. Therefore you have the \textbf{bibliography.bib} Bib\TeX\ file, where each piece of literature you want to cite has a so-called bib item. These look like:

\begin{minted}{tex}
@article{lilalu,
	author = {Lilalu, Lo and Lolale, Li},
	title = {Colors in the rainbow},
	journal = {Journal of Geophysical Research: Oceans},
	volume = {0815},
	number = {12345},
	issn = {2156-2202},
	doi = {10.1039/2016JC192757},
	keywords = {colors, rainbow},
	year = {2016},
}
\end{minted}

or

\begin{minted}{tex}
@article{einstein_1905,
	author =   "Albert Einstein",
	title =    "{On} the electrodynamics of moving bodies",
	journal =  "Annalen der Physik",
	volume =   "322",
	number =   "10",
	pages =    "891--921",
	year =     "1905",
	doi =      "10.1002/andp.19053221004"
	keywords = "physics, einstein",
}

\end{minted}

First, you have to specify the type of literature.
This goes directly behind the @.
After the opening curly bracket \{ you indicate the bib item key, which you use in the citation commands.\\
You can find these bib items for most available literature online. 
Usually you can just download a bib file from the publishers page. 
Then you only need to add the bib item in the \textbf{bibliography.bib} file and it should work.\\
More details about the Bib\TeX\ syntax can be found on \url{http://bibtex.org/}.

\subsection{Appendix}
\label{studentxx:appendix}

Everything you think is important but not important enough to show up in your actual report you can write into the file \textbf{appendix.tex}. 
The appendix is started with a \verb|\chapter{Your appendix}| and a label which you should specifiy as \verb|studentxx:appendix| (replace xx with your id). For example,

\begin{minted}{tex}
A table of all the measurement settings is in the appendix (see \autoref{studentxx:appendix}).
\end{minted}

results in: A table of all the measurement settings is found in the appendix (see \autoref{studentxx:appendix}).\\

\textbf{Important information:} Citations like you learned in \autoref{sec:citations} about won't really work in the \textbf{appendix.tex} file. 
Even if they show up they might mess up the references in the final report. 
So don't use them and if you really need to, speak to your editor to find a solution.


\section{Good (typographic) \LaTeX\ practice}

\begin{itemize}
	\item Always write one sentence per line, since \LaTeX\ is still a scripting language.
	\item Enclose quotes into \verb|\enquote{my quotation}| (\enquote{my quotation}) instead of \texttt{"my quotation"} ("my quotation").
	\item Use proper spacing after abbreviations:\\
	\verb|St.~Martin| or \verb|St.\ Martin| (St.~Martin) instead of \verb|St. Martin| (St. Martin).
	\item Use vector graphics (pdf, eps) instead of bitmaps (jpg, png, etc.) for your figures.
	\item For figures or tables with a long caption, always include a short descriptive caption (compare \autoref{tab:testtable}) by using \\ \verb|\caption[short caption]{Long caption with full sentences.}|.
	\item Write tables in the \emph{booktabs} style (with \verb|\toprule| etc.\ and without vertical lines).
	\item Use a Bib\TeX\ file for your bibliography.\\
	Cite with \verb|\textcite|, \verb|\parencite| or \verb|\footcite|.
	\item For units containing expressions use the tools of the \verb|SIunitx| package:
	\begin{itemize}
		\item \verb|\SI{315}{\watt\per\square\meter\per\kelvin}| will result in \SI{315}{\watt\per\meter\per\kelvin}
		\item \verb|\si{\newton\per\square\centi\meter}| for units only: \si{\newton\per\square\centi\meter}
		\item \verb|\SIrange{0}{10}{\celsius}| yields \SIrange{0}{10}{\celsius}
		\item \verb|\ang{78;13;01.2}| for coordinates: \ang{78;13;01.2}
		\item Or, really simple, \verb|\SI{10}{cm}| (\SI{10}{cm}) just for the proper spacing
	\end{itemize}
	\item Equations are always referenced with parantheses around the actual number, e.g.\ with \verb|\eqref{eq:rand}| you get (\textcolor{red}{2.4}).
	\item Everything that is text should be written as text in \LaTeX. That means in specific, declare initials and similar in mathmode with \verb|\text{}|. Only variables are slanted. For example,
	\begin{itemize}
		\item $T_\text{model}$ (\verb|T_\text{model}|) instead of $T_{model}$ (\verb|T_{model}|).
		\item A slanted $k$ in $\sum_{k=1}^n x_k$, on the other hand, is correct.
	\end{itemize}
	\item Write times with a colon (12:30) instead of a dot (12.30)
\end{itemize}


\section{Get started}
After you agreed on a label standard in your course and everyone got a student number, the next three things are the first things you are going to do:
\begin{itemize}
	\item Copy the \textbf{studentxx} directory and rename the xx by your given number.
	\item Open your \textbf{main.tex} file and give the title of your project as a chapter name and specify the according label \verb|\label{studentxx:report}| by replacing the xx to your given number or change it to what you agreed on.
	Also specify the authors in the \verb|\chapterauthor{}| command.
	\item Open your \textbf{appendix.tex} file and also change the label here to your desired label.
\end{itemize}

%Allright, we guess that is enough information for the moment. 
%You might also find some of the information from this guideline in the first comment lines in each .tex file. 
%Any specific questions you should ask the editor and also make sure that all of your course knows the answer. 
%Open your \textbf{main.tex} and get started. 
%Good luck with your report! 



\section{Troubleshooting}
Use \texttt{lualatex} or \texttt{xelatex} for compiling your documents. This ensures proper handling of Unicode characters. 
Compilation of the \LaTeX\ code may also not work on the UNIS server. 
Please copy the template skeleton onto your local machine and compile it there. 
If that doesn't work either, upload it to \url{www.sharelatex.com}.

\clearpage
\section{Available Packages}
The following packages and their respective functions are available:

\begin{multicols}{3}
\footnotesize
\begin{itemize}
	\item abstract
	\item adjcalc
	\item adjustbox
	\item amsbsy
	\item amsfonts
	\item amsgen
	\item amsmath
	\item amsopn
	\item amssymb
	\item amstext
	\item amsthm
	\item appendix
	\item array
	\item babel
	\item biblatex
	\item blindtext
	\item bm
	\item booktabs
	\item calc
	\item caption
	\item cleveref
	\item collectbox
	\item csquotes
	\item draftwatermark
	\item enumitem
	\item etoolbox
	\item fancyvrb
	\item floatrow
	\item fontenc
	\item fontspec
	\item geometry
	\item graphicx
	\item hyperref
	\item libertine
	\item lineno
	\item lscape
	\item microtype
	\item minted
	\item multicol
	\item nameref
	\item nicefrac
	\item nowidow
	\item pgf
	\item placeins
	\item sansmath
	\item scrlayer-scrpage
	\item scrlayer
	\item showkeys
	\item siunitx
	\item subcaption
	\item subfiles
	\item textcomp
	\item tikz
	\item tocloft
	\item todonotes
	\item translator
	\item trimclip
	\item typearea
	\item url
	\item verbatim
	\item xcolor
\end{itemize}
\end{multicols}


\end{document}
