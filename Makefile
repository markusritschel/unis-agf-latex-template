#!/usr/bin/make -f

SHELL = /bin/sh


.PHONY: cleanup, clear_logs, compile, latex, compress, get_git

cleanup:
	@for ext in aux bbl bcf blg lof log_ lot out run.xml synctex.gz toc; \
	do \
	find . -name \*.$${ext} -type f -delete; \
	done; \
	find . \( -name ".DS_Store" -o -name "Thumbs.db" \) -type f -delete; \
	find . -name _minted\* -type d -exec rm -rf {} \;


	
clear_logs:
	find . -name \*.log -type f -delete
	
get_git:
	git clone git@bitbucket.org:markusritschel/unis-agf-latex-template.git .

d = master

compile:
	@cd ${d}; \
	xelatex -shell-escape main.tex; \
	biber main; \
	xelatex -shell-escape main.tex; \
	xelatex -shell-escape main.tex;

latex: compile cleanup
	mv ${d}/main.pdf ${d}_final.pdf

compress:
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -sOutputFile=${d}_compressed.pdf ${d}_final.pdf
