# LaTeX template for the AGF courses at UNIS

This LaTeX template should help students writing their reports in a common style/layout and merging them together easily.
This is being achieved by the use of the _subfiles_ package.
The structure of the template also ensures that there will be no conflicts regarding images or TeX files with the same name, since every report chapter (located in a _studentxx_ directory) is in its own self-contained environment.
Only the labels and references must be unique. That will be handled by a labeling system which should be given by the editors.

More details can be found in the README pdf file.

The template can be either downloaded or cloned from the [public repository](https://bitbucket.org/markusritschel/unis-agf-latex-template/) or used from the UNIS server.

## Structure of the template
The repository contains besides its README files two folders (_master_ and _studentxx_) and a _Makefile_.
The single reports get written in a respective copy of the _studentxx_ directory and will be linked/included in the _main.tex_ file inside the _master_ directory later.
Compiling of the respective report parts can be done with the makefile by calling `make latex d=<id>` in the terminal, with `<id>` being either _"master"_ or one of the _studentxx_ tokens.
The makefile has also routines to `compress` the created pdf file and to `cleanup` auxiliary files.

## Students / Single reports
Students write their reports in a new _studentxx_ directory. 
Just copy the _studentxx_ directory from the repository and rename it according to a system which should be set by the editors.
However, for compiling the LaTeX code, the directory _master_ in the main directory must be available, since the _main.tex_ file in each _studentxx_ subdirectory refers to it.
Each student directory contains an _images_ subdirectory where all the graphics should be moved to.
Besides that, a _bibliography.bib_ file which contains all the references for the respective chapter in [BibTeX format](http://www.bibtex.org/) as well as the _main.tex_ file where all the LaTeX content goes in.
In each studentxx/_main.tex_ the chapter has to be indicated (matching the title of the fieldwork project) as well as a chapter label which matches the identifier of the naming system used for the students directories.
The author name(s) should be given with the command `\chapterauthor`.
The actual content can be either directly written into the _main.tex_ or sourced out into several files which can be included in the _main.tex_ by the command `\subinput{filename}`. 
These files can also exist in subdirectories inside the student's folder.
If the students have material which shall go in the appendix, this should be put into an _appendix.tex_ file and gets included with the command `\subappendix{appendix}`.


## Editors / Final report
The editors only use the content of the _master_ directory.
Besides the _main.tex_ file one can find the _agf-latex.cls_ and _agf-latex.sty_ which both contain the backbone of the template and don't need to be touched any further.
Also, a file for creating the title page (_titlepage.tex_) as well as a configuration file (_config.tex_) can be found here.
The commands for the title page can be set in the configuration file.
In the header of the master/main.tex you find some comments regarding several options for this template.
The single reports of the students have to be included in the _master/main.tex_ by using the command `\subfile{../studentxx/main}`.
The same yields for appendices in the respective environment at the end of _main.tex_.
In the end, the final report can be compiled by calling the makefile with `make latex d=master`. The option `d=master` is the default value and therefore optional.


----

### Contribution guidelines ###
Any improvements are highly appreciated. Therefore, please talk to the authors and/or send a pull request.
If the suggestions seem to include helpful improvements, they will be implemented in the existing template.
Besides that, every group can feel free to clone or download the repository and modify it locally according to their purposes.


### Who do I talk to? ###
The author is Markus Ritschel and can be addressed via [e-mail](mailto:kontakt@markusritschel.de).
